'use strict';

const config = require('config');
const mongoConfig = config.get('mongo');
const telegramConfig = config.get('telegram');

const logger = require('pino')();
const { connect } = require('./src/database/mongo');

const Router = require('./src/routes');
const TeleBot = require('telebot');

const bot = new TeleBot(telegramConfig.token);
const router = new Router(bot);

connect(mongoConfig).catch(error => {
  logger.error(error);
  process.exit(1);
});

router.init();

bot.start();

'use strict';

const config = require('config');
const pryvokzalnaId = config.get('clanId').pryvokzalna;

const ClanService = require('../services/clan.service');

class ClanController {
  constructor() {
    this.service = new ClanService();
  }

  async getInfo(msg, props) {
    const id = props.match[1] || pryvokzalnaId;

    msg.reply.text('Получаєм інфо...', { parseMode: 'html' });
    try {
      const info = await this.service.getInfo(id);

      return msg.reply.text(info, { parseMode: 'html' });
    } catch (ex) {
      const message =
        'Шото пошло не так :( ' +
        `<pre>status: ${ex.statusCode}</pre>\n` +
        '<pre>action: /info</pre>';
      return msg.reply.text(message, { parseMode: 'html' });
    }
  }

  async getNewcomers(msg, props) {
    const id = props.match[1] || pryvokzalnaId;

    msg.reply.text('Получаєм інфо...', { parseMode: 'html' });
    try {
      const newcomers = await this.service.getNewcomers(id);
      const result = newcomers
        .map(m => {
          return `<b>Name</b>: <pre>${m.name}</pre>\n` +
            `<b>Joined</b>: <pre>${m.date}</pre>\n`;
        })
        .join('\n');
      return msg.reply.text(result, { parseMode: 'html' });
    } catch (ex) {
      const message =
        'Шото пошло не так :( ' +
        `<pre>status: ${ex.statusCode}</pre>\n` +
        '<pre>action: /info</pre>';
      return msg.reply.text(message, { parseMode: 'html' });
    }
  }
}

module.exports = ClanController;

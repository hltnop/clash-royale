'use strict';

const config = require('config');
const helpers = require('../utils/helpers');
const pryvokzalnaId = config.get('clanId').pryvokzalna;

const DonationsService = require('../services/donations.service');

class DonationsController {
  constructor() {
    this.service = new DonationsService();
  }

  async getDonations(msg, props) {
    const order = props.match[1];

    msg.reply.text('Получаєм інфо...', { parseMode: 'html' });
    try {
      const donations = await this.service.getDonations(pryvokzalnaId, order);

      return helpers.sendMultipleMessages(msg, donations);
    } catch (ex) {
      const message =
        'Шото пошло не так :( ' +
        `<pre>status: ${ex.statusCode}</pre>\n` +
        '<pre>action: /info</pre>';
      return msg.reply.text(message, { parseMode: 'html' });
    }
  }

  async getDonationsHistory(msg, props) {
    const days = props.match[1] || 0;

    msg.reply.text('Рахуєм донат...', { parseMode: 'html' });
    try {
      const donations = await this.service.getDonationsHistory(
        pryvokzalnaId, days
      );
      return helpers.sendMultipleMessages(msg, donations);
    } catch (ex) {
      const message =
        'Шото пошло не так :( ' +
        `<pre>status: ${ex.statusCode}</pre>\n` +
        '<pre>action: /donations</pre>';
      return msg.reply.text(message, { parseMode: 'html' });
    }
  }
}

module.exports = DonationsController;

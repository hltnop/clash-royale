'use strict';

class HelpController {
  constructor() {}

  getHelp(msg) {
    const commands = [
      '<b>Commands list:</b>\n',
      '',
      '<pre>/info</pre>',
      '<pre>/info {clanId}</pre>',
      '',
      '<pre>/newcomers</pre>',
      '',
      '<pre>/donations</pre>',
      '<pre>/donations {order}</pre>',
      '<pre>{order} - "отримав" | "віддав" | "різниця"</pre>',
      '<pre>{order} - "received" | "donated" | "delta"</pre>',
      '',
      '<pre>/donations_history</pre>',
      '<pre>/donations_history {weeks_ago}</pre>',
      '',
      '<pre>/war</pre>',
      '<pre>/war {days}</pre>',
      '',
      '<pre>/war_not_played</pre>',
      '<pre>/war_not_played {days}</pre>',
      '',
      '<pre>/war_stats</pre>',
      '<pre>/war_stats {days}</pre>',
      '',
    ];

    return msg.reply.text(commands.join('\n'), {
      parseMode: 'html',
    });
  }

  text(msg, props) {
    const text = props.match[1];
    return msg.reply.text(`<b>This is</b>: <pre>${text}</pre>`, {
      parseMode: 'html',
    });
  }
}

module.exports = HelpController;

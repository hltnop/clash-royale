'use strict';

const config = require('config');
const helpers = require('../utils/helpers');
const pryvokzalnaId = config.get('clanId').pryvokzalna;

const WarService = require('../services/war.service');

class WarController {
  constructor() {
    this.service = new WarService();
  }

  async getInfo(msg, props) {
    const days = props.match[1] || 1;

    msg.reply.text('Рахуємо зіграні КВ...', { parseMode: 'html' });
    try {
      const warLogs = await this.service.getWarLogByDays(pryvokzalnaId, days);
      return helpers.sendMultipleMessages(msg, warLogs);
    } catch (ex) {
      const message =
        'Шото пошло не так :( ' +
        `<pre>status: ${ex.statusCode}</pre>\n` +
        '<pre>action: /war</pre>';
      return msg.reply.text(message, { parseMode: 'html' });
    }
  }

  async getNotPlayed(msg, props) {
    const days = props.match[1] || 1;

    msg.reply.text('Рахуємо не зіграні КВ...', { parseMode: 'html' });
    try {
      const warLogs = await this.service.getWarNotPlayed(pryvokzalnaId, days);
      return helpers.sendMultipleMessages(msg, warLogs);
    } catch (ex) {
      const message =
        'Шото пошло не так :( ' +
        `<pre>status: ${ex.statusCode}</pre>\n` +
        '<pre>action: /war_not_played$</pre>';
      return msg.reply.text(message, { parseMode: 'html' });
    }
  }

  async getStatistic(msg, props) {
    const wars = props.match[1];

    msg.reply.text('Рахуємо статистику КВ...', { parseMode: 'html' });
    try {
      const warLogs = await this.service.getStatistic(pryvokzalnaId, wars);
      return msg.reply.text(warLogs, { parseMode: 'html' });
    } catch (ex) {
      const message =
        'Шото пошло не так :( ' +
        `<pre>status: ${ex.statusCode}</pre>\n` +
        '<pre>action: /war_not_played$</pre>';
      return msg.reply.text(message, { parseMode: 'html' });
    }
  }
}

module.exports = WarController;

'use strict';

const mongoose = require('mongoose');
const logger = require('pino')();

mongoose.Promise = global.Promise;

mongoose.connection.once('open', () => {
  logger.info('Connected to MongoDB');
});

async function connect(mongoConfig) {
  try {
    await mongoose.connect(mongoConfig);
  } catch (error) {
    logger.error(error);
    process.exit(1);
    throw error;
  }
}

async function disconnect() {
  return mongoose.disconnect();
}

// if node process stops -> disconnect from mongo db
process.on('SIGINT', async function() {
  try {
    await disconnect();
    process.exit(-1);
  } catch (error) {
    process.exit(-1);
  }
});

module.exports = {
  connect,
  disconnect,
};

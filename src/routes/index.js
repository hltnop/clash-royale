'use strict';

const HelpController = require('../controllers/help.controller');
const ClanController = require('../controllers/clan.controller');
const DonationsController = require('../controllers/donations.controller');
const WarController = require('../controllers/war.controller');

class Router {
  constructor(bot) {
    this.bot = bot;

    this.HelpController = new HelpController();
    this.ClanController = new ClanController();
    this.DonationsController = new DonationsController();
    this.WarController = new WarController();
  }

  init() {
    this.bot.on(/^\/help(?:@pryvokzalna_bot)?$/, this.HelpController.getHelp);
    this.bot.on(/^\/text(?:@pryvokzalna_bot)? (.+)$/, this.HelpController.text);

    this.bot.on(
      [
        /^\/info(?:@pryvokzalna_bot)?$/,
        /^\/info(?:@pryvokzalna_bot)? (.+)$/,
      ],
      async(msg, props) => await this.ClanController.getInfo(msg, props)
    );
    this.bot.on(
      [
        /^\/newcomers(?:@pryvokzalna_bot)?$/,
        /^\/newcomers(?:@pryvokzalna_bot)? (.+)$/,
      ],
      async(msg, props) => await this.ClanController.getNewcomers(msg, props)
    );

    this.bot.on(
      [
        /^\/donations(?:@pryvokzalna_bot)?$/,
        /^\/donations(?:@pryvokzalna_bot)? (.+)$/,
      ],
      async(msg, props) => {
        return await this.DonationsController.getDonations(msg, props);
      }
    );
    this.bot.on(
      [
        /^\/donations_history(?:@pryvokzalna_bot)?$/,
        /^\/donations_history(?:@pryvokzalna_bot)? (.+)$/,
      ],
      async(msg, props) => {
        return await this.DonationsController.getDonationsHistory(msg, props);
      }
    );

    this.bot.on(
      [
        /^\/war(?:@pryvokzalna_bot)?$/,
        /^\/war(?:@pryvokzalna_bot)? (.+)$/,
      ],
      async(msg, props) => await this.WarController.getInfo(msg, props)
    );
    this.bot.on(
      [
        /^\/war_stats(?:@pryvokzalna_bot)?$/,
        /^\/war_stats(?:@pryvokzalna_bot)? (.+)$/,
      ],
      async(msg, props) => await this.WarController.getStatistic(msg, props)
    );
    this.bot.on(
      [
        /^\/war_not_played(?:@pryvokzalna_bot)?$/,
        /^\/war_not_played(?:@pryvokzalna_bot)? (.+)$/,
      ],
      async(msg, props) => await this.WarController.getNotPlayed(msg, props)
    );
  }
}

module.exports = Router;

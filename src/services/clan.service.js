'use strict';

const _ = require('lodash');
const moment = require('moment');
const RoyalAPIService = require('./royaleapi.service');

class ClanService {
  constructor() {}

  async getInfo(id) {
    const info = await RoyalAPIService.getClanInfo(id);

    const formatedResult =
      `<b>Name</b>: <pre>${info.name}</pre>\n` +
      `<b>Description</b>: <pre>${info.description}</pre>`;

    return formatedResult;
  }

  async getNewcomers(id) {
    const history = await RoyalAPIService.getClanHistory(id);
    let result = [];
    let converted = [];

    for (const date in history) {
      if (history.hasOwnProperty(date)) {
        const info = history[date];
        info.date = moment(date, 'YYYY-MM-DD-hh-mm').format('YYYY MM DD');
        converted.push(info);
      }
    }

    converted = converted.reverse();

    for (let i = 0; i < converted.length; i++) {
      const current = converted[i];
      const next = converted[i + 1];

      if (next) {
        const diff = _.differenceBy(current.members, next.members, 'tag');

        if (diff.length) {
          result = result.concat(
            diff.map((m) => {
              m.date = current.date;
              return m;
            })
          );
        }
      }
    }

    return result
  }
}

module.exports = ClanService;

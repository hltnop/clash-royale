'use strict';

const moment = require('moment');
const helpers = require('../utils/helpers');
const htmlEscape = require('escape-html');
const RoyalAPIService = require('./royaleapi.service');
const ClanService = require('./clan.service');

class DonationsService {
  constructor() {
    this.ClanService = new ClanService();
  }

  async getDonations(id, order) {
    const info = await RoyalAPIService.getClanInfo(id);
    const newcomers = await this.ClanService.getNewcomers(id);

    const members = info.members.filter(m => {
      return !newcomers.find(n => n.tag === m.tag);
    });

    const zero = helpers.sortDonationsBy(
      members.filter(member => member.donations === 0),
      order
    );
    const belowZero = helpers.sortDonationsBy(
      members.filter(member => {
        return member.donations && member.donationsDelta < 0;
      }),
      order
    );
    const aboveZero = helpers.sortDonationsBy(
      members.filter(member => {
        return member.donations && member.donationsDelta >= 0;
      }),
      order
    );

    const result = [];

    result.push(
      '<b>Список тих в кого різниця &gt;= 0</b> \n\n' +
      aboveZero.map(member => helpers.applyDonationsTpl(member)).join('\n')
    );

    result.push(
      '<b>Список тих в кого різниця &lt; 0</b> \n\n' +
      belowZero.map(member => helpers.applyDonationsTpl(member)).join('\n')
    );

    result.push(
      '<b>Список тих в кого донат = 0</b> \n\n' +
      zero.map(member => helpers.applyDonationsTpl(member)).join('\n')
    );

    return result;
  }

  async getDonationsHistory(id, days) {
    const history = await RoyalAPIService.getClanHistory(id);

    let converted = [];
    const result = [];

    for (const date in history) {
      if (history.hasOwnProperty(date)) {
        const info = history[date];
        info.date = date;
        converted.push(info);
      }
    }

    converted = converted.reverse();
    let prev = {};

    for (let i = 0; i < converted.length; i++) {
      const current = converted[i];

      if (!prev.donations || current.donations > prev.donations) {
        prev = current;
        result.push(current);
      }
    }

    days = days ? (days > result.length ? result.length : days) : result.length;
    let formatedResult = [];

    // i === 1 - Skip current week
    for (let i = 1; i < days; i++) {
      const day = result[i];
      const date = moment(day.date, 'YYYY-MM-DD-hh-mm').format('YYYY MM DD');
      let text = '';

      text += `<b>===== ${date} =====</b>\n\n`;
      text += day.members
        .sort((a, b) => a.donations - b.donations)
        .map(member => {
          return (
            `<b>${htmlEscape(member.name)}</b>\n` +
            `<pre>віддав: ${member.donations}</pre>\n`
          );
          // `<pre>отримав: ${member.donationsReceived}</pre>\n`;
        })
        .join('\n');

      formatedResult.push(text);
    }

    return formatedResult;
  }
}

module.exports = DonationsService;

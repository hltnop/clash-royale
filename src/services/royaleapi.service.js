'use strict';

const request = require('request-promise-native');
const config = require('config');

const API_TOKEN = config.get('clashapi.token');

/**
 * This endpoint retrieves a specific clan.
 *
 * @docs https://docs.royaleapi.com/#/endpoints/clan
 * @param {string} clanID - Clan hash
 * @returns {Promise<*>}
 */
const getClanInfo = async(clanID) => {
  return await request.get(
    `https://api.royaleapi.com/clan/${clanID}`,
    {
      headers: {auth: API_TOKEN},
      json: true,
    }
  );
};

/**
 * This endpoint returns a time series of member stats.
 * Clan stats are updated every 4 hours using UTC timezone.
 * Currently it is enabled for a selected set of clans which meet our internal criteria.
 *
 * @docs https://docs.royaleapi.com/#/endpoints/clan_history
 * @param {string} clanID - Clan hash
 * @returns {Promise<*>}
 */
const getClanHistory = async(clanID) => {
  return await request.get(
    `https://api.royaleapi.com/clan/${clanID}/history`,
    {
      headers: {auth: API_TOKEN},
      json: true,
    }
  );
};

/**
 * This endpoint returns data about the past clan wars.
 *
 * @docs https://docs.royaleapi.com/#/endpoints/clan_warlog
 * @param {string} clanID - Clan hash
 * @returns {Promise<*>}
 */
const getWarLog = async(clanID) => {
  return await request.get(
    `https://api.royaleapi.com/clan/${clanID}/warlog`,
    {
      headers: { auth: API_TOKEN },
      json: true,
    }
  );
};

module.exports = {
  getClanInfo,
  getClanHistory,
  getWarLog,
};

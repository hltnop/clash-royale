'use strict';

const moment = require('moment');
const htmlEscape = require('escape-html');

const RoyalAPIService = require('./royaleapi.service');

class WarService {
  constructor() {

  }

  async getWarLogByDays(id, days) {
    const warLog = await RoyalAPIService.getWarLog(id);

    let result = [];
    days = days
      ? days > warLog.length
        ? warLog.length
        : days
      : warLog.length;

    for (let i = 0; i < days; i++) {
      const warDay = warLog[i];
      const date = moment.unix(warDay.createdDate).format('YYYY MM DD');
      let text = '';

      text += `<b>===== ${date} =====</b>\n\n`;
      text += warDay.participants
        .map(player => {
          return (
            `<b>${htmlEscape(player.name)}</b>:\n` +
            `<pre>зібрано карт: ${player.cardsEarned}</pre>\n` +
            `<pre>зіграно битв: ${player.battlesPlayed}</pre>\n` +
            `<pre>виграно битв: ${player.wins}</pre>\n`
          );
        })
        .join('\n');

      result.push(text);
    }

    return result;
  }

  async getWarNotPlayed(id, days) {
    const warLog = await RoyalAPIService.getWarLog(id);

    const result = [];
    days = days
      ? days > warLog.length
        ? warLog.length
        : days
      : warLog.length;

    for (let i = 0; i < days; i++) {
      const warDay = warLog[i];
      const date = moment.unix(warDay.createdDate).format('YYYY MM DD');
      let text = '';

      text += `<b>===== ${date} =====</b>\n`;
      text += warDay.participants
        .filter(player => player.battlesPlayed === 0)
        .map(player => {
          return `<b>${htmlEscape(player.name)}</b>\n`;
        })
        .join('\n');

      result.push(text);
    }

    return result;
  }

  async getStatistic(id, wars) {
    const warLog = await RoyalAPIService.getWarLog(id);

    const result = {};
    wars = wars
      ? wars > warLog.length
        ? warLog.length
        : wars
      : warLog.length;

    for (let i = 0; i < wars; i++) {
      const warDay = warLog[i];

      warDay.participants.forEach((p) => {
        if (result[p.tag]) {
          result[p.tag].wins += p.wins;
          result[p.tag].loss += p.battlesPlayed - p.wins;
          result[p.tag].cards += p.cardsEarned;
        } else {
          result[p.tag] = {
            name: p.name,
            wins: p.wins,
            loss: p.battlesPlayed - p.wins,
            cards: p.cardsEarned,
          };
        }
      });
    }

    const converted = [];
    let res = [];

    for (const tag in result) {
      converted.push(result[tag]);
    }

    converted.sort((a, b) => b.wins - a.wins);

    res = converted.map((player) => {
      return `<b>Name</b>: ${htmlEscape(player.name)} \n` +
        `<b>wins</b>: ${player.wins} \n` +
        `<b>loss</b>: ${player.loss} \n` +
        `<b>cards</b>: ${player.cards} \n`;
    });
    res.unshift(`=== Статистика за ${wars} війн === \n`);
    res.push(`=== Статистика за ${wars} війн === `);

    return res.join('\n');
  }
}

module.exports = WarService;

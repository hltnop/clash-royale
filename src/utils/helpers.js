'use strict';

const q = require('q');

const sendMultipleMessages = (msg, data) => {
  return data.reduce((when, item) => {
    return when.then(() => msg.reply.text(item, {parseMode: 'html'}));
  }, q.when());
};

const sortDonationsBy = (donations, order) => {
  if (order === 'отримав' || order === 'received') {
    return donations.sort((a, b) => b.donationsReceived - a.donationsReceived);
  } else if (order === 'віддав' || order === 'donated') {
    return donations.sort((a, b) => b.donations - a.donations);
  } else if (order === 'різниця' || order === 'delta') {
    return donations.sort((a, b) => a.donationsDelta - b.donationsDelta);
  }

  return donations;
};

const applyDonationsTpl = (member) => {
  return `<b>${member.name}</b>\n` +
    `<pre>віддав: ${member.donations}</pre>\n` +
    `<pre>отримав: ${member.donationsReceived}</pre>\n` +
    `<pre>різниця: ${member.donationsDelta}</pre>\n`;
};


module.exports = {
  sendMultipleMessages,
  sortDonationsBy,
  applyDonationsTpl,
};
